# GNU screen  

## GNU screen とは  
仮想端末管理マネージャ(端末マルチプレクサ)。  
- 1つの端末の中に複数の仮想端末(ウィンドウ)を立ち上げて作業できる
- 端末の状態を保存・復元できる(デタッチ・アタッチ)
- 画面分割できる

### Official Website  
- https://www.gnu.org/software/screen/  

## Configure  
(~/.screenrc)
```
defutf8 on
defencoding utf8
encoding utf8 utf8
escape ^Tt
startup_message off
vbell off
hardstatus alwayslastline "%{=b wb} %H %{= wk}%-Lw%{= kr}%n%f* %t%{= wk}%+Lw"
autodetach on
defscrollback 100000
termcapinfo xterm* ti@:te@
logfile $HOME/logs/%Y%m%d%c%s-%n.log
deflog flush secs
deflog off
```

### エスケープキャラクタ  
デフォルトは [control + a (^A)] ですが、上記の設定で [control + t (^T)] に変更している。  

## 基本的なセッション・ウィンドウ操作  
- screen の起動  
```
$ screen
```  
- 起動時にセッション名を付ける
```
$ screen -S %session-name%
```
- 起動済みセッションに名前を付ける  
```
^T + :      #コマンドモードに入る
(command mode):sessionname %session-name%
```
- 新しいウィンドウを起動  
```
^T + c
```
- 新しいウィンドウに移動  
```
^T + t      #直前のウィンドウに移動
```
- 次のウィンドウに移動  
```
^T + n
```
- 前のウィンドウに移動  
```
^T + p
```
- 数字で指定したウィンドウに移動  
```
^T + %window number%
```
- ウィンドウを終了  
```
^T + k
```

## コピーモード  
- コピーモード
```
^T + [      #コピーモードに入る
```
- (コピーモード中)画面半分戻る
```
^u [control + u]
```
- (コピーモード中)画面半分進む  
```
^d [control + d]
```
- (コピーモード中)カーソル移動    
```
j : 1行進む
k : 1行戻る
^ : 先頭へ移動
$ : 行末へ移動
/%search word% : ワード検索
```
- (コピーモード中)コピー  
```
Space でコピー範囲の始点マーク、カーソル移動して Space でコピー範囲の終点確定
```
- コピーモード終了  
```
Esc
```
- ペースト  
```
^T + ]
```

## デタッチ・アタッチ(ウィンドウ状態の保存と復元)  
- デタッチ  
```
^T + d
```
- セッション確認(セッション一覧)  
```
$ screen -ls
```
- アタッチ  
```
$ screen -r     #デタッチ済みセッションが1つの場合
$ screen -r %session number% or %session name%
```
- アタッチ済みのセッションに、別端末からアタッチ(セッションの共有)  
```
$ screen -rx
```

## 画面分割  
- 水平分割  
```
^T + S
```
- 垂直分割  
```
^T + |
```
- 分割されたウィンドウに移動  
```
^T + tab
```
- 画面分割をやめる  
```
^T + Q
```
