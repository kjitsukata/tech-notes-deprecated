# Zsh  

[[_TOC_]]

## What's Zsh?  
Zsh is a shell designed for interactive use, although it is also a powerful scripting language.  

### Official Website  
- https://www.zsh.org/  

## How to install Zsh  

### macOS  
```
$ brew install zsh
```  
Add `/usr/local/bin/zsh` to `/etc/shells` file.

## How to customize Zsh    
- Plugin Manager : [Oh-My-Zsh](https://github.com/ohmyzsh/ohmyzsh "Oh-My-Zsh")  
- Theme : [Powerlevel10k](https://github.com/romkatv/powerlevel10k "Powerlevel10k")  

### Fonts installation  

#### macOS  
```
$ cd ~/Library/Fonts  
$ curl -OL https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf  
$ curl -OL https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf  
$ curl -OL https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf  
$ curl -OL https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf  
```  
Change the terminal fonts to `MesloLGS NF`\.  

###  Oh-My-Zsh installation  
```
$ zsh  
% sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"  
Cloning Oh My Zsh...  
.....  
Do you want to change your default shell to zsh? [Y/n] y  
.....  
Shell successfully changed to '/usr/local/bin/zsh'.

         __                                     __
  ____  / /_     ____ ___  __  __   ____  _____/ /_
 / __ \/ __ \   / __ `__ \/ / / /  /_  / / ___/ __ \
/ /_/ / / / /  / / / / / / /_/ /    / /_(__  ) / / /
\____/_/ /_/  /_/ /_/ /_/\__, /    /___/____/_/ /_/
                        /____/                       ....is now installed!


Before you scream Oh My Zsh! please look over the ~/.zshrc file to select plugins, themes, and options.

• Follow us on Twitter: https://twitter.com/ohmyzsh
• Join our Discord server: https://discord.gg/ohmyzsh
• Get stickers, shirts, coffee mugs and other swag: https://shop.planetargon.com/collections/oh-my-zsh  
```

### Powerlevel10k installation  
```  
% git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```  

`powerlevel10k` is cloned to `~/.oh-my-zsh/custom/themes/`\.  
Edit `ZSH_THEME` variable of `~/.zshrc` file\.  

```
ZSH_THEME="powerlevel10k/powerlevel10k"
```  

Restart zsh (or Restart terminal)\.  

### Powerlevel10k configuration  
Start configuration wizard\.  
If configuration wizard does not start, run `p10k configure` command\.  
Powerlevel10k configuration is saved in `~/.p10k.zsh`\.  

```
This is Powerlevel10k configuration wizard. It will ask you a few questions and
                             configure your prompt.

                Does this look like a diamond (rotated square)?
                  reference: https://graphemica.com/%E2%97%86

                                 --->    <---

(y)  Yes.

(n)  No.

(q)  Quit and do nothing.

Choice [ynq]:
```  

## Reference URL  

- [Zsh](https://www.zsh.org/ "Zsh")  
- [Oh-My-Zsh](https://github.com/ohmyzsh/ohmyzsh "Oh-My-Zsh")
- [Powerlevel10k](https://github.com/romkatv/powerlevel10k "Powerlevel10k")
