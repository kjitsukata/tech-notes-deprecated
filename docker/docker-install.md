# Docker  

## Install Docker Desktop on Mac  

[Homebrew](https://brew.sh/)でインストールします。  
```bash
$ brew install --cask docker
```
インストール後、Docker.appを起動します。  

### アップデート  

```bash
$ brew update
$ brew upgrade --cask docker
```

### Reference  

- [Docker Desktop for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac)
- Homebrew使わない方法: [Install Docker Desktop on Mac](https://docs.docker.com/docker-for-mac/install/)

## Docker version and working check  

```bash
$ sudo docker -v
Docker version 20.10.5, build 55c4c88
$ sudo docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Build with BuildKit (Docker Inc., v0.5.1-docker)
  scan: Docker Scan (Docker Inc., v0.6.0)

Server:
 Containers: 0
  Running: 0
  Paused: 0
  Stopped: 0
 Images: 0
 Server Version: 20.10.5
.....
```

## Hello World  

```bash
$ docker run hello-world
.....

Hello from Docker!
This message shows that your installation appears to be working correctly.

.....
```
